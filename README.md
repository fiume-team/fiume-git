Fiume Team Test Git
===================

**Step 1**

```cd <your folder>``` 

**Step 2**

```git init```

**Step 3**

```git remote add origin git@bitbucket.org:fiume-team/fiume-git.git```

**Step 4**

```git pull origin master```

Guide
---
http://rogerdudler.github.io/git-guide/

*check contributors.txt for contributors list*